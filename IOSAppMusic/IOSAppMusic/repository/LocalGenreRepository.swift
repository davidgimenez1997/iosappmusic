//
//  LocalGenreRepository.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 22/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit
import RealmSwift

class LocalGenreRepository: Repository {
    
    func getAll() -> [MGenre] {
        var tasks: [MGenre] = []
        do {
            let entities = try Realm().objects(EGenre.self).sorted(byKeyPath: "id", ascending: true) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.genreModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    func getNotAll() -> [MGenre] {
        var tasks: [MGenre] = []
        do {
            let entities = try Realm().objects(EGenre.self).filter("id != '0'") //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.genreModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    func get(identifier: String) -> MGenre? {
        do {
            let realm =  try Realm()
            if let entity = realm.objects(EGenre.self).filter("id == %@", identifier).first{ //Esto equivaldria a una Query
                let model = entity.genreModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: MGenre) -> Bool {
        do {
            let realm = try Realm()
            let entity = EGenre(id: a.id!, name: a.name!, songs: a.songs)
            try realm.write {
                realm.add(entity, update: true)
            }
        }
        catch {
            return false
        }
        
        return true
    }
    
    func update(a: MGenre) -> Bool {
        return create(a: a)
    }
    
    func delete(a: MGenre) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(EGenre.self).filter("id == %@", a.id) //Esto equivaldria a una Query
                realm.delete(entityToDelete)
            }
            
        }
        catch {
            return false
        }
        return true
    }
}
