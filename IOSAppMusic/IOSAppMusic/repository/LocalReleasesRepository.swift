//
//  LocalReleasesRepository.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 11/2/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import RealmSwift

class LocalReleasesRepository: Repository {
    
    func getAll() -> [MRelease] {
        var tasks: [MRelease] = []
        do {
            let entities = try Realm().objects(ERelease.self).sorted(byKeyPath: "id", ascending: true) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.releaseModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    func getByGenres(genre:String) -> [MRelease] {
        var tasks: [MRelease] = []
        do {
            let entities = try Realm().objects(ERelease.self).filter("genre == %@",genre) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.releaseModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    func getByCartStatus() -> [MRelease] {
        var tasks: [MRelease] = []
        do {
            let entities = try Realm().objects(ERelease.self).filter("isIntheCart == true") //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.releaseModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    
    
    
    func get(identifier: String) -> MRelease? {
        do {
            let realm =  try Realm()
            if let entity = realm.objects(ERelease.self).filter("id == %@", identifier).first{ //Esto equivaldria a una Query
                let model = entity.releaseModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: MRelease) -> Bool {
        do {
            let realm = try Realm()
            let entity = ERelease(release: a)
            try realm.write {
                realm.add(entity, update: true)
            }
        }
        catch {
            return false
        }
        
        return true
    }
    
    func update(a: MRelease) -> Bool {
        return create(a: a)
    }
    
    func delete(a: MRelease) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(ERelease.self).filter("id == %@", a.id) //Esto equivaldria a una Query
                realm.delete(entityToDelete)
            }
            
        }
        catch {
            return false
        }
        return true
    }
}
