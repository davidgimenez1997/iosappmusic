//
//  LocalSongsRepository.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//


import UIKit
import RealmSwift

class LocalSongsRepository: Repository {
    
    func getAll() -> [MSong] {
        var tasks: [MSong] = []
        do {
            let entities = try Realm().objects(ESong.self).sorted(byKeyPath: "name", ascending: true) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.songModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    func getByGenres(genre:String) -> [MSong] {
        var tasks: [MSong] = []
        do {
            let entities = try Realm().objects(ESong.self).filter("genre == %@",genre) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.songModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }

    func get(identifier: String) -> MSong? {
        do {
            let realm =  try Realm()
            if let entity = realm.objects(ESong.self).filter("id == %@", identifier).first{ //Esto equivaldria a una Query
                let model = entity.songModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: MSong) -> Bool {
        do {
            let realm = try Realm()
            let entity = ESong(song: a)
            try realm.write {
                realm.add(entity, update: true)
            }
        }
        catch {
            return false
        }
        
        return true
    }
    
    func update(a: MSong) -> Bool {
        return create(a: a)
    }
    
    func delete(a: MSong) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(ESong.self).filter("id == %@", a.id) //Esto equivaldria a una Query
                realm.delete(entityToDelete)
            }
            
        }
        catch {
            return false
        }
        return true
    }
}
