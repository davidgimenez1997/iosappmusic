//
//  LocalAlbumsRepository.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//


import UIKit
import RealmSwift

class LocalAlbumsRepository: Repository {
    
    func getAll() -> [MAlbum] {
        var tasks: [MAlbum] = []
        do {
            let entities = try Realm().objects(EAlbum.self).sorted(byKeyPath: "name", ascending: true) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.albumModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    
    func getByGenres(genre:String) -> [MAlbum] {
        var tasks: [MAlbum] = []
        do {
            let entities = try Realm().objects(EAlbum.self).filter("genre == %@",genre) //Esto equivaldria a una Query
            for entity in entities {
                let model = entity.albumModel()
                tasks.append(model)
            }
        }
        catch let error as NSError {
            print("Error getAll Tasks: ", error.description)
        }
        return tasks
    }
    
    
    func get(identifier: String) -> MAlbum? {
        do {
            let realm =  try Realm()
            if let entity = realm.objects(EAlbum.self).filter("id == %@", identifier).first{ //Esto equivaldria a una Query
                let model = entity.albumModel()
                return model
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    func create(a: MAlbum) -> Bool {
        do {
            let realm = try Realm()
            let entity = EAlbum(id: a.id!, name: a.name!, image: a.image!, description: a.description!, artist: a.artist!, genre: a.genre!, songs: a.songs, isIntheCart: a.isIntheCart, price: a.price!)
            try realm.write {
                realm.add(entity, update: true)
            }
        }
        catch {
            return false
        }
        
        return true
    }
    
    func update(a: MAlbum) -> Bool {
        return create(a: a)
    }
    
    func delete(a: MAlbum) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                let entityToDelete = realm.objects(EAlbum.self).filter("id == %@", a.id) //Esto equivaldria a una Query
                realm.delete(entityToDelete)
            }
            
        }
        catch {
            return false
        }
        return true
    }
}
