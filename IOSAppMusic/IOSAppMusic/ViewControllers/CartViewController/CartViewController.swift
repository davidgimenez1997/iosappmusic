//
//  CartViewController.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 09/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    // creamos las variables de la vista

    @IBOutlet weak var cartTableView: UITableView!
    
    @IBOutlet weak var cartAlbumTableView: UITableView!
    
    @IBOutlet weak var lblFinalPrice: UILabel!
    
    internal var finalPrice:Double? = 0.0
    
    // llamamos a lo metodos de registro de las celdas, calculamos
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Cart"
        registerCellSongs()
        registerCellAlbums()
        finalPrice = calcSongsPrice() + calcAlbumsPrice()
        setPrice()

    }
    
//    añadimos la celda a su tabla correspondiente
    internal func registerCellSongs(){
        let nib = UINib(nibName: "CartTableViewCell", bundle: nil)
        cartTableView.register(nib, forCellReuseIdentifier: "CartTableViewCell" )
    }
    
    internal func registerCellAlbums(){
        let nib = UINib(nibName: "CartAlbumsTableViewCell", bundle: nil)
        cartAlbumTableView.register(nib, forCellReuseIdentifier: "CartAlbumsTableViewCell" )
    }
    
//    setea el precio final a la variable price
    func setPrice(){
        
        if let price = finalPrice{
            lblFinalPrice.text = String(format: "%.2f", price)
            
        }
    }
    
//    calcula el precio final de las canciones, sumando la cancion añadida al total
    
    func calcSongsPrice() -> Double{
        var priceSongs:Double = 0.0
        
        for i in Session.shared.arrCartSongs{
            
            if let price = i.price{
                priceSongs = priceSongs + price
            }
        }
        
        return priceSongs
        
    }
    
    
    //    calcula el precio final de los albumes, sumando el album añadido al total

    func calcAlbumsPrice() -> Double{
        
        var priceAlbums:Double = 0.0
        
        for i in Session.shared.arrCartAlbums{
            
            if let price = i.price{
                priceAlbums = priceAlbums + price
            }
        }
        
        return priceAlbums
    }
    
//    funcion que resetea a 0 el total cuando se compran las canciones
    @IBAction func buyCart(_ sender: Any) {
        Session.shared.arrCartSongs = []
        Session.shared.arrCartAlbums = []
        ToastView.shared.long(self.view, txt_msg: "Thank you for your purchase")
        cartTableView.reloadData()
        cartAlbumTableView.reloadData()
          lblFinalPrice.text = String(format: "%.2f", 0.0)
    }
}

//extension de la tabla del carrito
extension CartViewController: UITableViewDelegate, UITableViewDataSource{
    
//  funcion que calcula cuantas columnas hay, segun items haya en el array
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.cartTableView){
                return Session.shared.arrCartSongs.count
        }else{
               return Session.shared.arrCartAlbums.count
        }
    }
    
//    titulos de las tablas
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(tableView == self.cartTableView){
            return "Songs"
        }else{
            return "Albums"
        }
    }
    
//    funcion que rellena las celdas de la tabla
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//    if para diferenciar si se añade un album o una cancion al carrito
        if(tableView == self.cartTableView){
//    se añaden los valores de la celda, accediendo al singleton (Session) y al array correspondiente
        let cell = cartTableView.dequeueReusableCell(withIdentifier: "CartTableViewCell") as! CartTableViewCell
            cell.lblSongCart.text = Session.shared.arrCartSongs[indexPath.row].name
            if let price = Session.shared.arrCartSongs[indexPath.row].price{
                cell.lblSongPriceCart.text = String(format: "%.2f", price)
            }
        return cell
            
        }else{
        let cell = cartAlbumTableView.dequeueReusableCell(withIdentifier: "CartAlbumsTableViewCell") as! CartAlbumsTableViewCell
            cell.lblAlbumsName.text = Session.shared.arrCartAlbums[indexPath.row].name
            if let price = Session.shared.arrCartAlbums[indexPath.row].price{
                cell.lblAlbumsPrice.text = String(format: "%.2f", price)
            }
        return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        funcion para añadir un estilo de borrar a la celda
        if editingStyle == .delete {
            
//            resetea el precio cuando se borra una cancion
            if(tableView == cartTableView){
                Session.shared.deleteSong(index: indexPath.row)
                finalPrice = calcSongsPrice() + calcAlbumsPrice()
                if let price = finalPrice{
                 lblFinalPrice.text = String(format: "%.2f", price)
                }
                cartTableView.reloadData()
                
//            resetea el precio cuando se borra un album

            }else{
                Session.shared.deleteAlbum(index: indexPath.row)
                finalPrice = calcSongsPrice() + calcAlbumsPrice()
                if let finalPrice = Session.shared.arrCartAlbums[indexPath.row].price{
                    
                    lblFinalPrice.text = String(format: "%.2f", finalPrice)
                }
                cartAlbumTableView.reloadData()
                
            }
            
        }
        
    }
    
}
