//
//  MainViewController.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var twitterBtn: UIButton!
    // creamos las variables para las imagenes de la pantalla de home
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var instaBtn: UIButton!
    
    // metodos que detectan pulsacion en la imagen y te envian a su respectiva url
    @IBAction func instagram(sender: UIButton) {
        if let url = NSURL(string: "https://www.instagram.com/musicmarketapp/") {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func twitter(sender: UIButton) {
        if let url = NSURL(string: "https://twitter.com/musicmarketapp1/status/1093603330637615105?s=12") {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func facebook(sender: UIButton) {
        if let url = NSURL(string: "https://www.facebook.com/Musicmarketapp-402289467212273/?modal=admin_todo_tour") {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    init(){
        // seteamos el icono y el titulo del tabBar
        super.init(nibName: "MainViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "home")
        self.title = NSLocalizedString("Home", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // seteamos titulo de la pantalla
        title = "Home"
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

}
