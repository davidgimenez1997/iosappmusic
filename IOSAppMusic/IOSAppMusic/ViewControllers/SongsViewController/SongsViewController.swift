//
//  SongsViewController.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class SongsViewController: UIViewController {
    let homeIcon = UIImage(named: "home")

    // creamos los array para las canciones y generos
    internal var songs:[MSong] = []
    internal var genres:[MGenre] = []
    @IBOutlet weak var collectionGenres: UICollectionView!
    
    @IBOutlet weak var collectionSongs: UICollectionView!
    
    
    // este metodo nos crea los dos botones de la appBar, uno para home y otro para el carrito
    private func setupBarButtonsItems(){
        let homeBarButton = UIBarButtonItem(title: "home", style: .plain, target: self, action: #selector(homeDirecction))
        homeBarButton.image = homeIcon
        navigationItem.setLeftBarButton(homeBarButton, animated: false)
        
        let cartBarButton = UIBarButtonItem(title: "cart", style: .plain, target: self, action: #selector(cartDirecction))
        cartBarButton.image = #imageLiteral(resourceName: "cart")
        navigationItem.setRightBarButton(cartBarButton, animated: true)
    }
    // metodo que se llama en uno de lo botones de la appBar y te redirige al home
    @objc func homeDirecction(){
        tabBarController?.selectedIndex = 0
    }
    // metodo que se llama en uno de lo botones de la appBar y te redirige al carrito
    @objc func cartDirecction(){
        
        let cartVC = CartViewController()
        navigationController?.pushViewController(cartVC, animated: true)
        
    }
    
    init(){
        super.init(nibName: "SongsViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "songsIcon")
        self.title = NSLocalizedString("Songs", comment: "")
 
    }
 
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // creamos los botones de la appBat
        setupBarButtonsItems()
        // registramos la celda para las songs
        registerCellSongs()
        // registramos la celda para los generos
        registerCellGenres()
        // le ponemos padding a la coleccion
        collectionPadding()
        // rellenamos los arrays con datos de la base de datos usando los metodos de la clase Session
        songs = Session.shared.songsRepository.getAll()
        genres = Session.shared.genreRepository.getAll()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool){
        // este metodo se llama cada vez que la pantalla se muestra y en este caso recarga la coleccion de canciones
        collectionSongs.reloadData()
    }
    
     //metodos para registrar las celdas de canciones y de generos
    internal func registerCellSongs(){
        let nib = UINib(nibName: "SongsCollectionViewCell", bundle: nil)
        collectionSongs.register(nib, forCellWithReuseIdentifier: "SongsCollectionViewCell")
    }
    
    internal func registerCellGenres(){
        let nib = UINib(nibName: "GenresCollectionViewCell", bundle: nil)
        collectionGenres.register(nib, forCellWithReuseIdentifier: "GenresCollectionViewCell")
    }
    
    // metodo para ponerle padding a las celdas
    func collectionPadding(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        self.collectionSongs.setCollectionViewLayout(layout, animated: true)
        
    }
}

// extension de la clase SongsViewController en la cual tenemos todos los metodos de las dos colecciones
extension SongsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // el if diferencia entre las dos colecciones de la pantalla y devuelve en valor diferente en cada caso
        if(collectionView == self.collectionSongs){
            return songs.count
        }else{
            return genres.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // el if diferencia entre las dos colecciones de la pantalla y devuelve en valor diferente en cada caso
        if(collectionView == self.collectionSongs){
            
            // creamos la celda
        let cell = collectionSongs.dequeueReusableCell(withReuseIdentifier: "SongsCollectionViewCell", for: indexPath) as! SongsCollectionViewCell
            
            // creamos variable  para asegurar que el valor no es nulo
            if let duration = songs[indexPath.row].duration{
                
                // estos metodos comprueban si algun filtro esta activado y si esta activado , si la cancion cumple los requisitos del filtro, si es asi la cancion se pinta con un borde verde , si no la cancion se pinta con un borde naranja.
                if (Session.shared.arrFilters[0].enable == true && duration > 3){
                    
                    cell.layer.borderColor = UIColor.green.cgColor
                    cell.lblTime.text = String(format: "%.2f", duration)
                    
                } else if (Session.shared.arrFilters[1].enable == true && duration < 3){
                    
                    cell.layer.borderColor = UIColor.green.cgColor
                    cell.lblTime.text = String(format: "%.2f", duration)
                    
                }else{
                    
                    cell.layer.borderColor = UIColor.orange.cgColor
                    cell.lblTime.text = String(format: "%.2f", duration)
                }
            }
            
            if let image = songs[indexPath.row].image{
                cell.imgSongs.image = UIImage(named: image)
            }
            // esto se ejecuta simpre indiferentemente de si hay un filtro aplicado o no.
            cell.lblSongs.text = songs[indexPath.row].name
            cell.layer.borderWidth = 2
            cell.lblTime.layer.cornerRadius = 15.0
            cell.timeView.layer.cornerRadius = 20.0
            
        return cell
            
        }else{
            
            // esta celda es la de los generos
            let cell = collectionGenres.dequeueReusableCell(withReuseIdentifier: "GenresCollectionViewCell", for: indexPath) as! GenresCollectionViewCell
            
            cell.lblGenre.text = genres[indexPath.row].name
            
            return cell
        }
    }
    
    // los dos metodos sigientes metodos sirven para ajustar el tamaño y el padding de cada celda
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)//here your custom value for spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = (collectionView.frame.width / 2) - lay.minimumInteritemSpacing - 10
        
        return CGSize(width:widthPerItem, height:widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == self.collectionSongs){
            
            // este metodo permite que selecciones una celda y te salga un popUp con la informacion de la cancion
            let songsPopUpVC = SongsPopUpViewController(song: songs[indexPath.row])
            songsPopUpVC.modalTransitionStyle = .coverVertical
            songsPopUpVC.modalPresentationStyle = .overCurrentContext
            present(songsPopUpVC, animated: true, completion: nil)
            
        }else{
            if (indexPath.row == 0){
                
                // si el index es 0 "simpre va a ser filtro" te lleva a la pantalla de filtro
                let filtersVC = FiltersViewController()
                navigationController?.pushViewController(filtersVC, animated: true)
                
            }else if (indexPath.row == 1){
                // si el index es 1 "siempre va a ser all" actualiza la tabla con todas las canciones de la base de datos
                songs = Session.shared.songsRepository.getAll()
                collectionSongs.reloadData()
                
            }else{
                // cualquier otro index selecciona un genero y usando un metodo de la base de datos actualiza la tabla con las canciones de solo ese genero
                if let name = genres[indexPath.row].name{
                    songs = Session.shared.songsRepository.getByGenres(genre: name)
                    collectionSongs.reloadData()
                }
            }
            
        }
    }
}





