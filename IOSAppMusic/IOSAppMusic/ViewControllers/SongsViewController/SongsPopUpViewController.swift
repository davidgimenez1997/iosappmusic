//
//  SongsPopUpViewController.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 28/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class SongsPopUpViewController: UIViewController {

    // creamos variables de la vista junto con un objeto Song
    @IBOutlet weak var popUpView: UIView!
    internal var song: MSong?
    @IBOutlet weak var lblNameSong: UILabel!
    @IBOutlet weak var closeSongsPopUp: UIButton!
    @IBOutlet weak var imgSongPopUp: UIImageView!
    @IBOutlet weak var genreView: UIView!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var durationView: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblGenre: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    
    // el constructor de esta clase recibe un objeto de tipo song
    convenience init(song: MSong?) {
        self.init()
        self.song = song
    }
    
    // llamamos a dos metodos
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialValues()
        setCornerRadius()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // este metodo se ejecuta cada vez que la pantalla se muestre en este caso hace que la pantalla aparezca con una animacion y lo pone un difuminado a la vista de detras
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.8){
            self.view.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        }
    }
    
    func setInitialValues(){
        // con la song que nos llega del constructor seteamos los valores de la celda
        // estos metodos nos permites comprobar que la song y otras variables no sea nulas
        if let song = song{
            lblNameSong.text = song.name
            if let image = song.image {
                imgSongPopUp.image = UIImage(named: image)
            }
            lblGenre.text = song.genre
            if let duration = song.duration {
                lblDuration.text = String(format: "%.2f", duration)
            }
            btnAdd.setImage(#imageLiteral(resourceName: "cart"), for: UIControlState.normal)
        }
    }
    
    //este metodo nos permite ponerle a la vista una redondez en las esquinas
    func setCornerRadius(){
        
        genreView.layer.cornerRadius = 15.0
        addView.layer.cornerRadius = 15.0
        durationView.layer.cornerRadius = 15.0
        popUpView.layer.cornerRadius = 15.0
        imgSongPopUp.layer.cornerRadius = 15.0
        
    }
    
    // un boton el cual vuelve a la vista anterior
    @IBAction func closePopUp(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    

    // este boton añade la cancion seleccionada al array del la clase Session
    // y crea un toast usando la clase ToastView
    @IBAction func addCart(_ sender: Any) {
        
        if let song = song{
            Session.shared.addSong(song: song)
        }
        ToastView.shared.short(self.view, txt_msg: "Song added to your cart")
    }
}
