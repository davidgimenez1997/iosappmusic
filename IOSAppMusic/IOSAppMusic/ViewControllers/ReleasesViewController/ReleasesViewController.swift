//
//  ReleasesViewController.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class ReleasesViewController: UIViewController {

//    creamos la view de tipo iCarousel
    @IBOutlet weak var carouselReleases: iCarousel!
    @IBOutlet weak var lblName: UILabel!
    //    Creamos el array de releases
    internal var arrReleases: [MRelease] = []
    let homeIcon = UIImage(named: "home")
    
//    creamos el boton de home y de carrito en el app bar
    private func setupBarButtonsItems(){
        let homeBarButton = UIBarButtonItem(title: "home", style: .plain, target: self, action: #selector(homeDirecction))
        homeBarButton.image = homeIcon
        navigationItem.setLeftBarButton(homeBarButton, animated: false)
        
        let cartBarButton = UIBarButtonItem(title: "cart", style: .plain, target: self, action: #selector(cartDirecction))
        cartBarButton.image = #imageLiteral(resourceName: "cart")
        navigationItem.setRightBarButton(cartBarButton, animated: true)
    }
    
//    funcion para volver al home
    @objc func homeDirecction(){
        tabBarController?.selectedIndex = 0
    }
    
//    funcion para ir al carrito
    @objc func cartDirecction(){
        
        let cartVC = CartViewController()
        navigationController?.pushViewController(cartVC, animated: true)
        
    }
    
//    creacion de el icono novedades en el tab bar
    init(){
        super.init(nibName: "ReleasesViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "releasesIcon")
        self.title = NSLocalizedString("Releases", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrReleases = Session.shared.releasesRepository.getAll()
        setupBarButtonsItems()
        setCarouselSettings()
        // Do any additional setup after loading the view.
    }
    
//    caracteristicas del carrusel
    func setCarouselSettings(){
        carouselReleases.type = .rotary
        carouselReleases.contentMode = .redraw
        carouselReleases.isPagingEnabled = true
        carouselReleases.scrollSpeed = 0.4
        carouselReleases.stopAtItemBoundary = true
        carouselReleases.scrollToItemBoundary = true
        carouselReleases.ignorePerpendicularSwipes = true
        carouselReleases.centerItemWhenSelected = true
        carouselReleases.layer.cornerRadius = 15.0
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        switch (option)
        {
        case .wrap:
            return 1
            
        default:
            return value
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
// funcion que devuelve cuantos items hay en el carrusel
extension ReleasesViewController: iCarouselDelegate, iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        if(arrReleases.count == 0){
            arrReleases = Session.shared.releasesRepository.getAll()
            return arrReleases.count
        }else{
            return arrReleases.count
        }
        
    }
    
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var imageView: UIImageView!
        if view == nil {
//            seteamos la imagen con su anchura y altura
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 40, height: 300))
            imageView.contentMode = .scaleAspectFit
        } else {
            imageView = view as? UIImageView
        }
//        cambia la imagen dependiendo en el index que este
        if let image = arrReleases[index].image{
            imageView.image = UIImage(named: image)
        }
        return imageView
    }
    
//    funcion para añadir un link a cada imagen del carrusel
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        if let url = arrReleases[index].link{
            let finalUrl = NSURL(string: url)
            UIApplication.shared.openURL(finalUrl as! URL)
        }
    }
    }

