//
//  addViewController.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 18/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

protocol AddViewControllerDelegate: class {
    func addViewController(_ vc: AddViewController, didEditAlbum album: MAlbum)
}

class AddViewController: UIViewController {
    
    internal var album: MAlbum!
    weak var delegate: AddViewControllerDelegate!
    
    @IBAction func saveButtonPressed() {
        album.name = "cosas"
        delegate.addViewController(self, didEditAlbum: album)
        
    }
    
    
}
