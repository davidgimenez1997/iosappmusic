//
//  FiltersViewController.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 11/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class FiltersViewController: UIViewController {
    
    // variable para la coleccion
    @IBOutlet weak var filtersCollectionView: UICollectionView!
    let cleanIcon = UIImage(named: "clean")

    // llamamos al metodo para registrar las celdas y para poner los botones del appBar
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCellFilter()
        setupBarButtonsItems()
    }
    
    // metodo para registrar las celdas
    internal func registerCellFilter(){
        let nib = UINib(nibName: "FiltersCollectionViewCell", bundle: nil)
        filtersCollectionView.register(nib, forCellWithReuseIdentifier: "FiltersCollectionViewCell")
    }
    
    // metodo para crear el appBar con los botones
    private func setupBarButtonsItems(){
        let cleanButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(cleanFilters))
        cleanButton.image = cleanIcon
//        homeBarButton.image = homeIcon
        navigationItem.setRightBarButton(cleanButton, animated: false)
    
    }
    
    // este metodo limpia todos los filtros
    @objc func cleanFilters(){
        
        for filter in Session.shared.arrFilters{
            
            filter.enable = false
        }
        self.filtersCollectionView.reloadData()
    }
    
}
extension FiltersViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Session.shared.arrFilters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = (collectionView.frame.width / 2) - lay.minimumInteritemSpacing - 10
        
        return CGSize(width:widthPerItem, height:widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = filtersCollectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as! FiltersCollectionViewCell
        
        cell.nameLbl.text = Session.shared.arrFilters[indexPath.row].name
        if Session.shared.arrFilters[indexPath.row].enable == true{
            cell.nameLbl.backgroundColor = UIColor.green
        }else{
            cell.nameLbl.backgroundColor = UIColor.red
        }
        return cell
    }
    // este meto te permite pinchando en celda activar el filtro
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Session.shared.arrFilters[indexPath.row].enable = true
        self.filtersCollectionView.reloadData()
    }
    
}
