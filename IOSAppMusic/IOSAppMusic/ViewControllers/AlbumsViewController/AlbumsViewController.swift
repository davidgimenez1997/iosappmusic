//
//  AlbumsViewController.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class AlbumsViewController: UIViewController {

    // creamos variables para las colecciones y los dos arrays de generos y albumes
     let homeIcon = UIImage(named: "home")
    @IBOutlet weak var collectionAlbums: UICollectionView!
    @IBOutlet weak var collectionGenres: UICollectionView!
    internal var albums:[MAlbum] = []
    internal var genres:[MGenre] = []
    
    init(){
        // seteamos todos los datos de tabBar
        super.init(nibName: "AlbumsViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "albumsIcon")
        self.title = NSLocalizedString("Albums", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    // metodo que se llama en uno de los botones de la appbar y que nos lleva a la pantalla del carrito
    @objc func cartDirecction(){
        let cartVC = CartViewController()
        navigationController?.pushViewController(cartVC, animated: true)
    }
    
    // creamos el appbar y metemos dos botones en el mismo, uno para el boton que nos llevara a la pantalla de home y el otro a la pantalla de carrito
    private func setupBarButtonsItems(){
        let homeBarButton = UIBarButtonItem(title: "home", style: .plain, target: self, action: #selector(homeDirecction))
        homeBarButton.image = homeIcon
        navigationItem.setLeftBarButton(homeBarButton, animated: false)
        
        let cartBarButton = UIBarButtonItem(title: "cart", style: .plain, target: self, action: #selector(cartDirecction))
        cartBarButton.image = #imageLiteral(resourceName: "cart")
        navigationItem.setRightBarButton(cartBarButton, animated: true)
    }
    
    // metodo para agregar padding a las celdas
    func collectionPadding(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        self.collectionAlbums.setCollectionViewLayout(layout, animated: true)
    }
    
    // metodos para registrar las celdas de albumes y de generos
    internal func registerCellAlbums(){
        let nib = UINib(nibName: "AlbumsCollectionViewCell", bundle: nil)
        collectionAlbums.register(nib, forCellWithReuseIdentifier: "AlbumsCollectionViewCell")
    }
    
    internal func registerCellGenres(){
        let nib = UINib(nibName: "GenresCollectionViewCell", bundle: nil)
        collectionGenres.register(nib, forCellWithReuseIdentifier: "GenresCollectionViewCell")
    }
    
    // metodo para que el boton del appBar te lleve a la pantalla de home
    @objc func homeDirecction(){
       tabBarController?.selectedIndex = 0
    }
    
    // llamamos a los metodos de registrar las celdas y al de crear el appBar, usando los metodos de la clase Session metemos todos los albumen y generos en sus respectivos arrays
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBarButtonsItems()
        registerCellAlbums()
        registerCellGenres()
        collectionPadding()
        albums = Session.shared.albumRepository.getAll()
        genres = Session.shared.genreRepository.getNotAll()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension AlbumsViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //el if comprueba que coleccion esta modificando
        if (collectionView == self.collectionAlbums){
            return albums.count
        }else{
            
            return genres.count
        }
        
    }
    
    // metodos para ponerle padding y margenes a las celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)//here your custom value for spacing
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = (collectionView.frame.width / 2) - lay.minimumInteritemSpacing - 10
        
        return CGSize(width:widthPerItem, height:widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //el if comprueba que coleccion esta modificando
        if (collectionView == self.collectionAlbums){
            
            let cell = collectionAlbums.dequeueReusableCell(withReuseIdentifier: "AlbumsCollectionViewCell", for: indexPath) as! AlbumsCollectionViewCell
            cell.albumsLblName.text = albums[indexPath.row].name
            cell.lblGenre.text = albums[indexPath.row].genre
            //comprueba que la imagen no sea null
            if let image = albums[indexPath.row].image{
                cell.albumsImage.image = UIImage(named: image)
            }
            return cell
        }
        else{
            
            let cell = collectionGenres.dequeueReusableCell(withReuseIdentifier: "GenresCollectionViewCell", for: indexPath) as! GenresCollectionViewCell
            
            cell.lblGenre.text = genres[indexPath.row].name
            cell.lblGenre.textColor = UIColor.white
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == self.collectionGenres){
                // si el index es 0 "siempre va a ser all" actualiza la tabla con todas las canciones de la base de datos
            if (indexPath.row == 0){
                    albums = Session.shared.albumRepository.getAll()
                    collectionAlbums.reloadData()
            }else{
                // cualquier otro index selecciona un genero y usando un metodo de la base de datos actualiza la tabla con las canciones de solo ese genero
                if let name = genres[indexPath.row].name{
                    albums = Session.shared.albumRepository.getByGenres(genre: name)
                    collectionAlbums.reloadData()
                }
            }
        }else{
            // este metodo permite que selecciones una celda y te salga un popUp con la informacion de la cancion
            let albumsPopUpVC = AlbumsPopUpViewController(album: albums[indexPath.row])
            albumsPopUpVC.modalTransitionStyle = .coverVertical
            albumsPopUpVC.modalPresentationStyle = .overCurrentContext
            present(albumsPopUpVC, animated: true, completion: nil)
        }
    }
}
