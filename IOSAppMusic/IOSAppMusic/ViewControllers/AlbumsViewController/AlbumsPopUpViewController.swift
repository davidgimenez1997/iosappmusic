//
//  AlbumsPopUpViewController.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 07/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class AlbumsPopUpViewController: UIViewController {
    
    // creamos variables para el album que nos va a llegar por el constructor, un array de canciones y los botones de la vsita
    internal var album: MAlbum?
    internal var songs:[MSong] = []
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblSongsView: UIView!
    @IBOutlet weak var imageAlbums: UIImageView!
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var songsTableView: UITableView!
    
    
    // llamamos a los metodos para registrar las celdas, setear los datos en la view y cambiar de manera visual la view.
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCellSongs()
        setData()
        visualChanges()
        // Do any additional setup after loading the view.
    }
    
    internal func visualChanges(){
        
        btnAdd.setImage(#imageLiteral(resourceName: "cart"), for: UIControlState.normal)
        lblSongsView.layer.cornerRadius = 15.0
        
    }
    
    //registramos la celda
    internal func registerCellSongs(){
        let nib = UINib(nibName: "SongsTableViewCell", bundle: nil)
        songsTableView.register(nib, forCellReuseIdentifier: "SongsTableViewCell" )
    }
    
    //al constructor le llega una variable album
    convenience init(album: MAlbum?) {
        self.init()
        self.album = album
    }
    
    // seteamos los datos usando la variable que nos llega del constructor
    func setData(){
        if let album = album{
            lblAlbumName.text = album.name
            songs = album.songs
            
            if let album = album.image{
                imageAlbums.image = UIImage(named: album)
            }
        }
        
    }

    // boton que nos permite salir de la view
    @IBAction func btnDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // boton que nos permite usando un metodo de la clase Session añadir un album al carrito
    @IBAction func btnAdd(_ sender: Any) {
        if let album = album{
            Session.shared.addAlbum(album: album)
        }
        ToastView.shared.short(self.view, txt_msg: "Album added to your cart")
    }
    
}

// extension en la que mostramos la tabla con las canciones del album
extension AlbumsPopUpViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = songsTableView.dequeueReusableCell(withIdentifier: "SongsTableViewCell") as! SongsTableViewCell
        cell.lblSongs.text = songs[indexPath.row].name
        return cell
    } 
}
