//
//  PlaylistViewController.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class PlaylistViewController: UIViewController {

    let homeIcon = UIImage(named: "home")
    
    init(){
        super.init(nibName: "PlaylistViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "playlistIcon")
        self.title = NSLocalizedString("Playlists", comment: "")
    }
    
    private func setupBarButtonsItems(){
        let homeBarButton = UIBarButtonItem(title: "home", style: .plain, target: self, action: #selector(homeDirecction))
        homeBarButton.image = homeIcon
        navigationItem.setLeftBarButton(homeBarButton, animated: false)
        
        let cartBarButton = UIBarButtonItem(title: "cart", style: .plain, target: self, action: #selector(cartDirecction))
        cartBarButton.image = #imageLiteral(resourceName: "cart")
        navigationItem.setRightBarButton(cartBarButton, animated: true)
    }
    
    @objc func homeDirecction(){
        tabBarController?.selectedIndex = 0
    }
    @objc func cartDirecction(){
        
        let cartVC = CartViewController()
        navigationController?.pushViewController(cartVC, animated: true)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBarButtonsItems()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
