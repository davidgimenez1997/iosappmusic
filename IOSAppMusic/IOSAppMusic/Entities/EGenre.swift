//
//  EGenre.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 22/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import RealmSwift

class EGenre: Object{
    
    @objc dynamic var id: String? = ""
    @objc dynamic var name: String? = ""
    var songs: List<ESong> = List<ESong>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id:String,name:String,songs:[MSong]) {
        self.init()
        self.id = id
        self.name = name
        for song in songs {
            self.songs.append(song.songEntity())
        }
    }
    
    func genreModel() -> MGenre{
        let model = MGenre()
        model.id = self.id
        model.name = self.name
        for song in self.songs {
            model.songs.append(song.songModel())
        }
        return model
    }
}
