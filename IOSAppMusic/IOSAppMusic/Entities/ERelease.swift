//
//  ERelease.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 11/2/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import RealmSwift

class ERelease: Object {
    
    @objc dynamic var id: String? = ""
    @objc dynamic var image: String? = ""
    @objc dynamic var name: String? = ""
    @objc dynamic var link: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init (release: MRelease)
    {
        self.init()
        self.id = release.id
        self.name = release.name
        self.image = release.image!
        self.link = release.link!
    }
    func releaseModel() -> MRelease{
        let model = MRelease(id: self.id!, name: self.name!, image: self.image!, link: self.link)
        model.id = self.id
        model.name = self.name
        model.image = self.image
        model.link = self.link
        return model
    }
}
