//
//  Song.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import RealmSwift

class ESong: Object {
    
   @objc dynamic var id: String? = ""
   @objc dynamic var name: String? = ""
   @objc dynamic var image: String = ""
   @objc dynamic var duration: Double = 0
   @objc dynamic var descriptionSong: String? = ""
   @objc dynamic var artist: String? = ""
   @objc dynamic var genre: String? = ""
   @objc dynamic var isIntheCart : Bool = false
   @objc dynamic var price : Double = 0
   @objc dynamic var explicit : Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    convenience init (song: MSong)
    {
        self.init()
        self.id = song.id
        self.name = song.name
        self.image = song.image!
        self.duration = song.duration!
        self.descriptionSong = song.description!
        self.artist = song.artist
        self.genre = song.genre
        self.isIntheCart = song.isIntheCart
        self.price = song.price!
        self.explicit = song.explicit
    }
    func songModel() -> MSong{
        let model = MSong(id: self.id!, name: self.name!, image: self.image, duration: self.duration, description: self.descriptionSong!, artis: self.artist!, genre: self.genre!,isIntheCart: self.isIntheCart, price: self.price, explicit: self.explicit)
        model.id = self.id
        model.name = self.name
        model.image = self.image
        model.duration = self.duration
        model.description = self.descriptionSong
        model.artist = self.artist
        model.genre = self.genre
        model.isIntheCart = self.isIntheCart
        model.price = self.price
        model.explicit = self.explicit
        return model
    }
}
