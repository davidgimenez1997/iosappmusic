//
//  EAlbum.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import RealmSwift

class EAlbum: Object {
    
    @objc dynamic var id: String? = ""
    @objc dynamic var name: String? = ""
    @objc dynamic var descriptionAlbum: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var artist: String? = ""
    @objc dynamic var genre: String? = ""
    var songs: List<ESong> = List<ESong>()
    @objc dynamic var isIntheCart: Bool = false
    @objc dynamic var price: Double = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id:String,name:String,image:String,description:String,artist:String,genre:String,songs:[MSong],isIntheCart:Bool, price:Double) {
        self.init()
        self.id = id
        self.name = name
        self.image = image
        self.artist = artist
        self.genre = genre
        self.isIntheCart = isIntheCart
        self.price = price
        for song in songs {
            self.songs.append(song.songEntity())
        }
    }
 
    func albumModel() -> MAlbum{
        let model = MAlbum()
        model.id = self.id
        model.name = self.name
        model.image = self.image
        model.description = self.descriptionAlbum
        model.artist = self.artist
        model.genre = self.genre
        model.isIntheCart = self.isIntheCart
        model.price = self.price
        for song in self.songs {
            model.songs.append(song.songModel())
        }
        return model
    }
}
