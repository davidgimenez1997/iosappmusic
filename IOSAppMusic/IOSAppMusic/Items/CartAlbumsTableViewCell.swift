//
//  CartAlbumsTableViewCell.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 10/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class CartAlbumsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAlbumsName: UILabel!
    
    @IBOutlet weak var lblAlbumsPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
