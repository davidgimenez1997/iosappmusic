//
//  CartTableViewCell.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 09/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSongCart: UILabel!
    
    @IBOutlet weak var lblSongPriceCart: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
