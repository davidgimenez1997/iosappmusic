//
//  AlbumsCollectionViewCell.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado Alvarez on 04/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class AlbumsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var albumsImage: UIImageView!
    @IBOutlet weak var albumsLblName: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
