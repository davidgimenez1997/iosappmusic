//
//  SongsCollectionViewCell.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 18/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import UIKit

class SongsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblSongs: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgSongs: UIImageView!
    @IBOutlet weak var timeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
