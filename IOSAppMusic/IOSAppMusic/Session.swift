//
//  Session.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 11/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

final class Session {
    private init() { }
    static let shared = Session()

//    creamos 3 diferentes arrays , dos para el carrito y uno para los filtros
    var arrCartSongs = [MSong]()
    var arrCartAlbums = [MAlbum]()
    var arrFilters = [MFilter]()
//    creamos variables para acceder a los diferentes repositorios
    var songsRepository = LocalSongsRepository()
    var albumRepository = LocalAlbumsRepository()
    var genreRepository = LocalGenreRepository()
    var releasesRepository = LocalReleasesRepository()
    
// este metodo nos permite añadir un objeto cancion al array arrCartSong
    func addSong(song:MSong){
        arrCartSongs.append(song)
    }
// este metodo nos permite borrar un objeto cancion al array arrCartSong
    func deleteSong(index:Int){
        arrCartSongs.remove(at: index)
    }
    
//    este metodo nos permite añadir un objeto album al array arrCartAlbum
    func addAlbum(album:MAlbum){
        arrCartAlbums.append(album)
    }
    
//    este metodo nos permite borrar un objeto album al array arrCartAlbum
    func deleteAlbum(index:Int){
        arrCartAlbums.remove(at: index)
    }
    
// este metodo nos comprube si la base de datos esta vacia, si lo esta crea unas variables y la introduce el la misma
    
    func checkData(){
        
//        if songsRepository.getAll() == nil{
        
//        creamos todos los objetos
        let papercut = MSong(id:"1", name: "Papercut", image: "papercut", duration: 3.05, description: "first released in 2000 in the album Hybrid theory", artis: "Linking Park", genre: "Rock", isIntheCart: false, price: 1.99, explicit: true)
        let crawling = MSong(id:"2", name: "Crawling", image: "Crawling", duration: 3.29, description: "Great song from a great music group", artis: "Linking Park", genre: "Rock", isIntheCart: false, price: 1.99, explicit: false)
        let intheend = MSong(id:"3", name: "In The End", image: "inTheEnd", duration: 3.36, description: "Another great hit from the group", artis: "Linking Park", genre: "Rock", isIntheCart: false, price: 1.99, explicit: false)
        let jahRuleTheWorld = MSong(id:"4", name: "Jah Rule The World", image: "jahRuleTheWorld", duration: 5.55, description: "grear song for just chill", artis: "Cali P", genre: "PsyTrance", isIntheCart: false, price: 1.99, explicit: false)
        let tornado = MSong(id:"5", name: "Tornado", image: "tornado", duration: 7.06, description: "great song for psytrance lovers", artis: "isolated", genre: "PsyTrance", isIntheCart: false, price: 1.99, explicit: true)
        let imposible = MSong(id:"6", name: "Imposible", image: "imposible", duration: 3.54, description: "great song for psytrance lovers", artis: "Luis Fonsi", genre: "Pop", isIntheCart: false, price: 1.99, explicit: false)
        let hellBack = MSong(id:"7", name: "Hell & Back", image: "hellBack", duration: 4.12, description: "great song for psytrance lovers", artis: "Kid Ink", genre: "Hiphop", isIntheCart: false, price: 1.99, explicit: true)
        let mujerBruja = MSong(id:"8", name: "Mujer Bruja", image: "mujerBruja", duration: 3.14, description: "great song for psytrance lovers", artis: "Miriam", genre: "Pop", isIntheCart: false, price: 1.99, explicit: false)
        let estaRico = MSong(id:"9", name: "Esta Rico", image: "estaRico", duration: 2.56, description: "great song for psytrance lovers", artis: "Luis Fonsi", genre: "Pop", isIntheCart: false, price: 1.99, explicit: false)
        let powerGlory = MSong(id:"10", name: "Power & Glory", image: "powerGlory", duration: 3.10, description: "great song for psytrance lovers", artis: "isolated", genre: "Powermetal", isIntheCart: false, price: 1.99, explicit: true)
        let cryThunder = MSong(id:"11", name: "Cry Thunder", image: "cryThunder", duration: 3.19, description: "great song for psytrance lovers", artis: "isolated", genre: "Powermetal", isIntheCart: false, price: 1.99, explicit: false)
        let nemo = MSong(id:"12", name: "Nemo", image: "nemo", duration: 2.34, description: "great song for psytrance lovers", artis: "isolated", genre: "Powermetal", isIntheCart: false, price: 1.99, explicit: true)
        let redNose = MSong(id:"13", name: "Red Nose", image: "redNose", duration: 3.54, description: "great song for psytrance lovers", artis: "Kid Kudi", genre: "Hiphop", isIntheCart: false, price: 1.99, explicit: false)
        let thePrayer = MSong(id:"14", name: "The Prayer", image: "thePrayer", duration: 2.15, description: "great song for psytrance lovers", artis: "Kid Kudi", genre: "Hiphop", isIntheCart: false, price: 1.99, explicit: true)
        let vasAQuedarte = MSong(id:"15", name: "Vas a Quedarte", image: "vasAQuedarte", duration: 3.23, description: "great song for psytrance lovers", artis: "Aitana", genre: "Pop", isIntheCart: false, price: 1.99, explicit: false)
        let closer = MSong(id:"16", name: "Closer", image: "closer", duration: 3.23, description: "great song for psytrance lovers", artis: "Aitana", genre: "Edm", isIntheCart: false, price: 1.99, explicit: true)
        
        let Filter = MGenre(id: "0", name: "Filter", songs: [])
        let all = MGenre(id: "1", name: "All", songs: [])
        let rock = MGenre(id: "2", name: "Rock", songs: [])
        let pop = MGenre(id: "3", name: "Pop", songs: [])
        let edm = MGenre(id: "4", name: "Edm", songs: [])
        let hiphop = MGenre(id: "5", name: "Hiphop", songs: [])
        let powermetal = MGenre(id: "6", name: "Powermetal", songs:[])
        let psytrance = MGenre(id: "7", name: "PsyTrance", songs:[])
        
        let HybridTheory = MAlbum(id: "1", name: "Hybrid Theory", image: "hybryd", description: "Released in europe in the year 2000", artis: "Linking Park", genre: "Rock", songs: [papercut,crawling,intheend], isIntheCart: false, price: 4.99)
        let desdeElBarrioConAmor = MAlbum(id: "2", name: "desde el barrio con amor", image: "conAmor", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Rock", songs: [nemo, redNose], isIntheCart:false, price: 4.99)
        let edmAlbum = MAlbum(id: "3", name: "DeadMau", image: "edmAlbum", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Edm", songs: [closer, jahRuleTheWorld], isIntheCart:false, price: 4.99)
        let edmAlbum1 = MAlbum(id: "4", name: "NeonFuture", image: "edmAlbum1", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Edm", songs: [jahRuleTheWorld, intheend, imposible], isIntheCart:false, price: 4.99)
        let edmAlbum2 = MAlbum(id: "5", name: "RushCar", image: "edmAlbum2", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Edm", songs: [], isIntheCart:false, price: 4.99)
        let felicesCuatro = MAlbum(id: "6", name: "Felices", image: "felicesCuatro", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Pop", songs: [mujerBruja, vasAQuedarte, estaRico], isIntheCart:false, price: 4.79)
        let hiphopAlbum = MAlbum(id: "7", name: "Cleveland", image: "hiphopAlbum", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Hiphop", songs: [thePrayer, hellBack], isIntheCart:false, price: 4.99)
        let hiphopAlbum2 = MAlbum(id: "8", name: "Hood", image: "hiphopAlbum2", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Hiphop", songs: [hellBack, nemo, powerGlory], isIntheCart:false, price: 4.99)
        let hiphopAlbum5 = MAlbum(id: "9", name: "Hell", image: "hiphopAlbum5", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Hiphop", songs: [tornado, closer], isIntheCart:false, price: 4.99)
        let juanes = MAlbum(id: "10", name: "Paseos", image: "juanes", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Pop", songs: [powerGlory, hellBack, crawling, papercut], isIntheCart:false, price: 4.99)
        let popAlbum = MAlbum(id: "11", name: "Juntos", image: "popAlbum", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Pop", songs: [imposible, estaRico], isIntheCart:false, price: 4.99)
        let popAlbum3 = MAlbum(id: "12", name: "Conoceme", image: "popAlbum3", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Pop", songs: [thePrayer, redNose, powerGlory], isIntheCart:false, price: 4.99)
        let powerMetalAlbum = MAlbum(id: "13", name: "High", image: "powerMetalAlbum", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Powermetal", songs: [closer, nemo, estaRico], isIntheCart:false, price: 4.99)
        let psytranceAlbum3 = MAlbum(id: "14", name: "Bullet", image: "psytranceAlbum3", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "PsyTrance", songs: [tornado, mujerBruja, intheend], isIntheCart:false, price: 4.99)
        let rapAlbum = MAlbum(id: "15", name: "HeadShot", image: "rapAlbum", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Rock", songs: [], isIntheCart:false, price: 4.99)
        let rapAlbum2 = MAlbum(id: "16", name: "W.O.W", image: "rapAlbum2", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Rock", songs: [], isIntheCart:false, price: 4.99)
        let rapAlbum4 = MAlbum(id: "17", name: "AK", image: "rapAlbum4", description: "primer album de pisoFranko", artis: "pisoFranko2", genre: "Rock", songs: [redNose, nemo, cryThunder], isIntheCart:false, price: 4.99)
        
        
        
        let releaseConcert = MRelease(id: "0", name: "Concerts", image: "concertsReleases", link: "https://www.ticketea.com/conciertos/madrid/?a_aid=TKTMKTG&a_bid=9f0126be&gclid=Cj0KCQiA7briBRD7ARIsABhX8aDR_Zu9i-nINwlDIX_SwyP096uRrSkZ09QCWu1nA0h8KvEv_Ip3oUsaAhdzEALw_wcB")
        let releaseSongs = MRelease(id: "1", name: "Songs", image: "songsReleases", link: "https://elgenero.com/canciones")
        let releaseAlbum = MRelease(id: "2", name: "Albums", image: "albumsReleases", link: "https://www.lahiguera.net/musicalia/novedades.php")
        
        let moreThat3 = MFilter(name:"+ 3m", enable: false)
        
        let lessThat3 = MFilter(name: "- 3m", enable:false)
        
        
//         hacemos el append al array de filtros de los 3 filtros creados previamente
        arrFilters.append(moreThat3)
        arrFilters.append(lessThat3)

//         añadimos todos los datos a la base de datos con creates
        
        releasesRepository.create(a: releaseConcert)
        releasesRepository.create(a: releaseSongs)
        releasesRepository.create(a: releaseAlbum)
            
        songsRepository.create(a: papercut)
        songsRepository.create(a: crawling)
        songsRepository.create(a: intheend)
        songsRepository.create(a: jahRuleTheWorld)
        songsRepository.create(a: tornado)
        songsRepository.create(a: imposible)
        songsRepository.create(a: hellBack)
        songsRepository.create(a: mujerBruja)
        songsRepository.create(a: estaRico)
        songsRepository.create(a: powerGlory)
        songsRepository.create(a: cryThunder)
        songsRepository.create(a: nemo)
        songsRepository.create(a: redNose)
        songsRepository.create(a: thePrayer)
        songsRepository.create(a: vasAQuedarte)
        songsRepository.create(a: closer)
        
        albumRepository.create(a: HybridTheory)
        albumRepository.create(a: desdeElBarrioConAmor)
        albumRepository.create(a: edmAlbum)
        albumRepository.create(a: edmAlbum1)
        albumRepository.create(a: edmAlbum2)
        albumRepository.create(a: felicesCuatro)
        albumRepository.create(a: hiphopAlbum)
        albumRepository.create(a: hiphopAlbum2)
        albumRepository.create(a: hiphopAlbum5)
        albumRepository.create(a: juanes)
        albumRepository.create(a: popAlbum)
        albumRepository.create(a: popAlbum3)
        albumRepository.create(a: powerMetalAlbum)
        albumRepository.create(a: psytranceAlbum3)
        albumRepository.create(a: rapAlbum)
        albumRepository.create(a: rapAlbum2)
        albumRepository.create(a: rapAlbum4)
        
        
        genreRepository.create(a: Filter)
        genreRepository.create(a: all)
        genreRepository.create(a: rock)
        genreRepository.create(a: pop)
        genreRepository.create(a: edm)
        genreRepository.create(a: hiphop)
        genreRepository.create(a: powermetal)
        genreRepository.create(a: psytrance)
        
        
    }
//    }
    
}
    

