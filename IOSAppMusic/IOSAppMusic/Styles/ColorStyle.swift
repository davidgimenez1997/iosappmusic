//
//  ColorStyle.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import UIKit

//colores para usar en la clase Style
class ColorStyle{
    class func tintColor() -> UIColor{
        return UIColor.orange
    }
    class func navigationBarTintColor() -> UIColor{
        return UIColor.black
    }
    
    
}
