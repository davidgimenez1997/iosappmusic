//
//  Style.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation
import UIKit

//clase para definir los colores y los estilos del tab bar y del app bar
class Style{
    class func customize(){
        UINavigationBar.appearance().barTintColor = ColorStyle.navigationBarTintColor()
        UITabBar.appearance().barTintColor = ColorStyle.navigationBarTintColor()
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
}
