//
//  MSong.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

class MSong{
    var id: String?
    var name: String?
    var image: String?
    var duration: Double?
    var description: String?
    var artist: String?
    var genre: String?
    var isIntheCart : Bool
    var price : Double?
    var explicit:Bool
    
    func songEntity() -> ESong {
        let entity = ESong()
        entity.id = self.id
        entity.name = self.name
        entity.image = self.image!
        entity.duration = self.duration!
        entity.descriptionSong = self.description
        entity.artist = self.artist
        entity.genre = self.genre
        entity.isIntheCart = self.isIntheCart
        entity.price = self.price!
        return entity
    }
    
    
    init(id:String,name:String,image:String,duration:Double,description:String,artis:String,genre:String,isIntheCart:Bool, price:Double , explicit:Bool) {
        self.id=id
        self.name=name
        self.image=image
        self.duration=duration
        self.description=description
        self.artist=artis
        self.genre=genre
        self.isIntheCart = isIntheCart
        self.price = price
        self.explicit = explicit
    }
 
 
}
