//
//  MRelease.swift
//  IOSAppMusic
//
//  Created by JORGE VAZQUEZ REQUEJO on 11/2/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

class MRelease{
    var id: String?
    var name: String?
    var image: String?
    var link: String?
    
    func releaseEntity() -> ERelease {
        let entity = ERelease()
        entity.id = self.id
        entity.name = self.name
        entity.image = self.image!
        entity.link = self.link!
        return entity
    }
    
    
    init(id:String,name:String,image:String,link:String) {
        self.id=id
        self.name=name
        self.image=image
        self.link=link
    }
}
