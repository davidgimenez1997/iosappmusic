//
//  MAlbum.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 15/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

class MAlbum{
    
    var id: String?
    var name: String?
    var image: String?
    var description: String?
    var artist: String?
    var genre: String?
    var songs:[MSong] = []
    var isIntheCart:Bool = false
    var price : Double?
    
    
    convenience init(id:String,name:String,image:String,description:String,artis:String,genre:String,songs:[MSong?], isIntheCart:Bool, price: Double) {
        self.init()
        self.id=id
        self.name=name
        self.image=image
        self.description=description
        self.artist=artis
        self.genre=genre
        self.isIntheCart = isIntheCart
        self.songs = songs as! [MSong]
        self.price = price
    }
    
    func toEntity() -> EAlbum {
        let entity = EAlbum(id: self.id!, name: self.name!, image: self.image!, description: self.description!, artist: self.artist!, genre: self.genre!, songs: self.songs, isIntheCart: self.isIntheCart, price: self.price!)
        return entity
    }
 
    
}
