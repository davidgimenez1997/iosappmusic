//
//  MFilter.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 13/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

class MFilter{
    var name :String?
    var enable:Bool
    
    
    init(name:String?,enable:Bool) {
        self.name = name
        self.enable = enable
    }
    
}
