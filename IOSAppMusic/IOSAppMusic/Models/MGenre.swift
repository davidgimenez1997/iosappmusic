//
//  MGenre.swift
//  IOSAppMusic
//
//  Created by PABLO GUARDADO ALVAREZ on 22/1/19.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

class MGenre{
    
    var id: String?
    var name: String?
    var songs:[MSong] = []
    
    convenience init(id:String,name:String,songs:[MSong?]) {
        self.init()
        self.id=id
        self.name=name
        self.songs = songs as! [MSong]
    }
    
}
