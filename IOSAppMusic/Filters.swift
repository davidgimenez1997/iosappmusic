//
//  Filters.swift
//  IOSAppMusic
//
//  Created by Pablo Guardado on 11/02/2019.
//  Copyright © 2019 PABLO GUARDADO ALVAREZ. All rights reserved.
//

import Foundation

class Filter{
    
    var biggerThan3: Bool?
    var smallerThan3: Bool?
    var explicit :Bool?
    
    init(biggerThan3:Bool?,smallerThan3:Bool?,explicit:Bool?) {
        self.biggerThan3 = biggerThan3
        self.smallerThan3 = smallerThan3
        self.explicit = explicit
        
    }
    
}
